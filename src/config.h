#ifndef CONFIG_H
#define CONFIG_H

#include "util.h"
#include <msx.h>
#include <sys/ioctl.h>
#include <conio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <eos.h>
#include <smartkeys.h>
#include "adamkeys.h"
#include "ui.h"
#include "screens.h"
#include "sound.h"

typedef struct GameConfig
{
    RadioOptionGroup gameMode;
    RadioOptionGroup setScoring;
    RadioOptionGroup pocketFarkle;
    RadioOptionGroup fujiNetMode;    
    CheckOption penalty;
    CheckOption toxic;
    char fujiNetHostName[256];
} GameConfig;

#define CONFIG_PF_1K  0
#define CONFIG_PF_300 1

#define MODE_COMPUTER 0
#define MODE_LOCAL_TWO_PLAYER 1
#define MODE_FUJINET 2

/**
 * @brief 
 * 
 * @param config Config struct
 */
void initGameConfig(GameConfig *config);

/**
 * @brief 
 * 
 * @param config 
 */
void optionsLoop(GameConfig *config);
#endif /* CONFIG_H */