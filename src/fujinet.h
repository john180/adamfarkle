/**
 * @file fujinet.h
 * @author John Wohlers (john@wohlershome.net)
 * @brief 
 * @version 0.1
 * @date 2022-02-12
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#ifndef FUJINET_H
#define FUJINET_H

#include <stdlib.h>
#include <msx.h>
#include <sys/ioctl.h>
#include <conio.h>
#include <stdbool.h>
#include <eos.h>
#include <smartkeys.h>
#include "util.h"
#include "screens.h"
#include "adamkeys.h"

#define FUJINET_DEVICE  0x0F
#define TAPE_1          0x08
#define NET 			0x09      // Adamnet ID for network device
#define ADAMNET_TIMEOUT 0x9B
#define STATUS_MASK     0x7F
#define ACK             0x80      // Return value for AdamNet ACK

#define ADAMNET_SEND_APPKEY_READ  0xDD
#define ADAMNET_SEND_APPKEY_WRITE 0xDE
#define MAX_APPKEY_LEN 64

#define FUJINET_MODE_HOST 0
#define FUJINET_MODE_GUEST 1

/**
 * @brief 
 * 
 * @return true 
 * @return false 
 */
bool isFujiNet(void);

/**
 * @brief UI for selecting / entering opponent
 * 
 * @param game_mode 
 * @param player1_name 
 * @param player2_name 
 * @return Returns true if this copy should act as host, false if we are the guest
 */
bool configFujiPlayer(char* hostname);

/**
 * @brief returns the lastN characters os a string
 * 
 * @param str  The string to truncate
 * @param n    How many characters
 */
char *lastNchar(const char *str, int n);

/**
 * @brief 
 * 
 * @param h 
 * @return true 
 * @return false 
 */
bool fujinetConnect(char *h);

/**
 * @brief 
 * 
 */
void fujinetListen(void);

/**
 * @brief 
 * 
 * @return true 
 * @return false 
 */
bool fujinetConnectionWaiting(void);

/**
 * @brief 
 * 
 */
void fujinetAcceptConnection(void);

/**
 * @brief 
 * 
 */
void fujinetCloseConnection(void);

/**
 * @brief 
 * 
 * @param buf 
 */
void fujinetSend(char *buf);

/**
 * @brief 
 * 
 * @param buf 
 */
void fujinetRecv(char *buf);

/**
 * @brief 
 * 
 * @param n 
 * @return true 
 * @return false 
 */
bool fujinetConnectToHost(char *n);

/**
 * @brief 
 * 
 * @return true 
 * @return false 
 */
bool fujinetListenForConnection(void);

/**
 * @brief Send a move to the opponent
 * 
 * @param sendPit 
 */
void fujinetSendPit(unsigned char sendPit);

/**
 * @brief 
 * 
 * @param creator 
 * @param app 
 * @param key 
 * @param buf 
 * @return unsigned char 
 */
unsigned char fujinetAppkeyRead(unsigned int creator, unsigned char app, unsigned char key, char *buf);

/**
 * @brief 
 * 
 * @param creator 
 * @param app 
 * @param key 
 * @param buf 
 * @return unsigned char 
 */
unsigned char fujinetAppkeyWrite(unsigned int creator, unsigned char app, unsigned char key, char *buf);

/**
 * @brief 
 * 
 * @param config 
 */
void fujinetMenuLoop(GameConfig *config);

/**
 * @brief 
 * 
 * @param dice_group 
 */
void fujinetSendDice(Die *dice_group);


/**
 * @brief 
 * 
 */
void fujinetSetupConnection(GameConfig *config, Player *players);


/**
 * @brief 
 * 
 * @param keycode 
 */
void fujinetSendKey(unsigned char keycode);

/**
 * @brief Send a code to display an animation
 * 
 */
void fujinetSendAnimation(int animation);

/**
 * @brief 
 * 
 * @param players 
 * @param turn 
 */
void fujinetSendScore(Player players[MAX_PLAYERS], int turn);

/**
 * @brief 
 * 
 */
void fujinetSendTaunt(int taunt);



#endif /* FUJINET_H */