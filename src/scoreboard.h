/**
 * @file scoreboard.h
 * @author John Wohlers (john@wohlershome.net)
 * @brief 
 * @version 0.1
 * @date 2022-02-22
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef SCOREBOARD_H
#define SCOREBOARD_H


#include "util.h"
#include <stdbool.h>
#include <msx.h>
#include <sys/ioctl.h>
#include <conio.h>
#include <stdlib.h>
#include "player.h"
#include "config.h"
#include "sound.h"

void updateScoreDisplay(Player players[MAX_PLAYERS], int turn);

int tallyRollScore(Die *dice_group, int roll, GameConfig *config);

#endif