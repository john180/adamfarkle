/**
 * @file mainmenu.h
 * @author John Wohlers (john@wohlershome.net)
 * @brief 
 * @version 0.1
 * @date 2022-03-03
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef MAINMENU_H
#define MAINMENU_H

#include "util.h"
#include <msx.h>
#include <sys/ioctl.h>
#include <conio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <eos.h>
#include <smartkeys.h>
#include "adamkeys.h"
#include "ui.h"
#include "screens.h"
#include "player.h"
#include "config.h"
#include "fujinet.h"
#include "sound.h"

/**
 * @brief 
 * 
 * @param config 
 * @param players 
 */
void mainMenuLoop(GameConfig *config, Player *players);


#endif // MAINMENU_H
