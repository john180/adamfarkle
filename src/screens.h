/**
 * @file screens.h
 * @author John Wohlers (john@wohlershome.net)
 * @brief 
 * @version 0.1
 * @date 2022-02-22
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef SCREENS_H
#define SCREENS_H


#include "sound.h"
#include "util.h"
#include "ui.h"
#include "config.h"
#include <msx.h>
#include <sys/ioctl.h>
#include <conio.h>
#include <stdlib.h>
#include "drawbox.h"
#include "drawlogo.h"
#include "dice.h"
#include "player.h"



#define ANIMATION_ROLL 0
#define ANIMATION_FARKLE 1
#define ANIMATION_TOXIC 2
#define ANIMATION_THREE_FARKLE 3
#define ANIMATION_WINNER 4

/**
 * @brief Draw the overall game board
 * 
 */
void drawGameBoard(void);

/**
 * @brief Update turn menu display
 * 
 * @param mode 
 */
void menuTurnOption(int mode);

/**
 * @brief Construct a new draw Player Names object
 * 
 * @param players
 */
void drawPlayerNames(Player *players);

/**
 * @brief Take an array of dice values and displays them in the save bar
 * 
 * @param dice 
 */
void drawBank(Die *dice);

/**
 * @brief 
 * 
 */
void drawCurrentPlayer(Player *players, short current_player);

/**
 * @brief 
 * 
 * @param players 
 * @param current_player 
 */
void drawMainMenuPlayerName(Player *players, short current_player);

/**
 * @brief 
 * 
 */
void drawGameOptionsScreen();


/**
 * @brief Main menu screen
 * 
 */
void drawMainMenu();


/**
 * @brief 
 * 
 * @param config 
 */
void drawFujiOpponentScreen();

/**
 * @brief Draws the playfield, also used to clear the palyfield after animations
 * 
 */
void drawPlayField(void);

/**
 * @brief Animation for Toxic Twos condition
 * 
 */
void animateToxicTwos(void);

/**
 * @brief Animation for Three Farkle condition
 * 
 */
void animateThreeFarkle(void);

/**
 * @brief 
 * 
 */
short drawInstructions(short start_line);

/**
 * @brief 
 * 
 * @param name 
 */
void animateWinner(char name[15]);

/**
 * @brief 
 * 
 */
void animateRolling();

/**
 * @brief 
 * 
 */
void animateFarkle();

/**
 * @brief 
 * 
 */
void doAnimation(int animation);

#endif