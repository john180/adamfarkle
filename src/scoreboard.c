/**
 * @file scoreboard.c
 * @author John Wohlers (john@wohlershome.net)
 * @brief 
 * @version 0.1
 * @date 2022-02-22
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include "scoreboard.h"



void updateScoreDisplay(Player players[MAX_PLAYERS], int turn) {


    char score[8];

    msx_color(INK_WHITE,INK_CYAN,INK_CYAN);

    sprintf(score, "%6d", players[0].score);
    gotoxy(23,6);
    cputs(score);

    sprintf(score, "%6d", players[1].score);
    gotoxy(23,9);
    cputs(score);

    sprintf(score, "%6d", turn);
    gotoxy(23,13);
    cputs(score);

}


int tallyRollScore(Die *dice_group, int roll, GameConfig *config) {
    bool valid = true;
    bool straight[6] = {false,false,false,false,false,false};
    int count[6] = {0,0,0,0,0,0};
    short pairs_count = 0;
    int score = 0;

    int i, j = 0;

    for (i=0; i < 6; i++) {
        
        if (dice_group[i].saved == true && dice_group[i].locked == false) {  // we only care about this roll... previously locked die were already tallied
            
            int val = dice_group[i].value;
            count[val - 1]++;
            straight[val-1] = true; 
        }
    }

    // check for 3 pairs
    for (i=0; i < 6; i++) {  
        if (count[i] == 6) { 
            pairs_count = 3;
            break; // no point in continuing counting we have 6 of a kind!
        }
        if (count[i] >= 4 && count[i] < 6) {
            pairs_count = pairs_count + 2;
        }
        if (count[i] >= 2 && count[i] < 4) {
            pairs_count++;
        }
    }

    for (i=0; i < 6; i++) {  // check for a straight
        if (straight[i] == false) valid = false;
    }
    if (valid == true ) {
        //TODO: Straight score based on config option
        score = 3000; // Wow, a straight! Don't bother running other tallys
    }
    else if (pairs_count == 3 ) {
        score = 1500;
    }
    else {
        //TODO: use config to modify this logic based on rules 
        if (count[0] < 3 ){
            score = score + (100 * count[0]);
        }
        if (count[4] < 3 ){
            score = score + (50 * count[4]);
        }

        if (count[0] >= 3 ){
            if (config->pocketFarkle.value == CONFIG_PF_1K) {
                score = score + 1000 + ((count[0] - 3) * 1000);
            }
            else {
                score = score + 300 + ((count[0] - 3) * 300);
            }
        }

        //TODO: Use config->setScoring to adjust values accordingly
        for (i=1;i<6;i++) {
            if (count[i] >= 3 ){
                score = score + ((i+1) * 100) + ((count[i] - 3) * (i+1) * 100);
            }
        }
    }
  
    return (score);
}