/**
 * @file gamelogic.h
 * @author John Wohlers (john@wohlershome.net)
 * @brief 
 * @version 0.1
 * @date 2022-02-22
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef LOGIC_H
#define LOGIC_H

#include "sound.h"
#include <stdlib.h>
#include <stdbool.h>
#include <msx.h>
#include <sys/ioctl.h>
#include <conio.h>
#include <smartkeys.h>
#include <eos.h>
#include "util.h"
#include "adamkeys.h"
#include "dice.h"
#include "screens.h"
#include "scoreboard.h"
#include "player.h"
#include "config.h"
#include "brain.h"
#include "fujinet.h"

typedef struct ValidMove
{
    bool valid;
    char reason[60];
} ValidMove;


void gameLoop(Player *players, GameConfig *config);

void checkValid(Die *dice_group, bool checkBanked, ValidMove *valid_move);

void checkToxic(Die *dice_group, ValidMove *valid_move);

#endif