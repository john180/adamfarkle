/**
 * @file screens.h
 * @author John Wohlers (john@wohlershome.net)
 * @brief  Reusable? UI elements for radio menu, check box item.
 * @version 0.1
 * @date 2022-02-22
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef UI_H
#define UI_H


#include "util.h"
#include <msx.h>
#include <sys/ioctl.h>
#include <conio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "sound.h"

typedef struct CheckOption  // TODO: use the generic item struct in place of this
{
    int x;
    int y;
    bool isChecked;
    int boxColor;
    int textColor;
    char label[31];
} CheckOption;

/**
 * @brief options that reside inside the radio option group and menu widgets
 * 
 */
typedef struct WidgetItem
{
    int x;
    int y;
    bool isSelected;
    int buttonColor;
    int textColor;
    char label[31];
} WidgetItem;

/**
 * @brief Group of radio button options
 * 
 */
typedef struct RadioOptionGroup
{
    int x;
    int y;
    int textColor;
    int num_options;
    int cols;
    int value;
    char pip_selected;
    char pip_unselected;
    char label[31];
    WidgetItem options[3];
} RadioOptionGroup;



/**
 * @brief 
 * 
 * @param x 
 * @param y 
 * @param boxColor 
 * @param textColor 
 * @param label 
 * @param value 
 * @param check_option 
 */
void initCheckOption(int x, int y, int boxColor, int textColor, char label[31], bool value, CheckOption *check_option) ;

/**
 * @brief draws the check box option on screen
 * 
 * @param check_option 
 */
void drawCheckOption(CheckOption *check_option);

/**
 * @brief Toggles teh state of the check box, and redraws the check box character
 * 
 * @param check_option 
 */
void toggleCheckOption(CheckOption *check_option);

void initRadioOptionGroup(int x, int y, int button_color, int text_color, int header_color, char label[31], int value, int num_options, int columns, char item_labels[6][31], char *pip_selected, char *pip_unselected, RadioOptionGroup *option_group);

/**
 * @brief 
 * 
 * @param x  // Horisontal Location
 * @param y  // Vertical Location
 * @param buttonColor  // UI element (radio | check) color
 * @param textColor  // Color of the header text
 * @param label  // Header text for this group
 * @param option  // Array of WidgetItem options
 */
void initRadioOption(int x, int y, int buttonColor, int textColor, char label[31], WidgetItem *option);

void drawRadioOptionGroup(RadioOptionGroup *option_group);

void toggleRadioOption(RadioOptionGroup *option_group);

void disableField(int x, int y, int len);
#endif /* UI_H */