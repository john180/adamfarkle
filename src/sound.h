/**
 * @file sound.h
 * @author John Wohlers (john@wohlershome.net)
 * @brief 
 * @version 0.1
 * @date 2022-03-17
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#ifndef FARKLE_SOUND_H
#define FARKLE_SOUND_H

#include <msx.h>
#include <eos.h>
#include <intrinsic.h>
#include <interrupt.h>
#include <arch/z80.h>

#define SOUND_DICE_ROLL 0
#define SOUND_SPROING 1
#define SOUND_THEME 2
#define SOUND_TOXIC_TWOS 3
#define SOUND_FARKLED 4
#define SOUND_COMMENT 5

void _play(unsigned char n);

static void farkle_nmi_play(void);

void farkle_sound_init(void);

void farkle_sound_play(int sound);

#endif /* FARKLE_SOUND_H */
