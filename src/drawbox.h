/**
 * @file drawbox.h
 * @author John Wohlers (john@wohlershome.net)
 * @brief Draws a box using user defined graphics characters.
 * @version 0.1
 * @date 2022-02-12
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef DRAWBOX_H
#define DRAWBOX_H

#include <msx.h>
#include <sys/ioctl.h>
#include <conio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "font.h"


/**
 * @brief Box drawing function
 * 
 * @param x  // X position
 * @param y  // Y position
 * @param w  // Width
 * @param h  // Height
 * @param clear  // Fill the box with spaces?
 * 
 */
void drawBox(int x, int y, int w, int h, bool clear);

#endif