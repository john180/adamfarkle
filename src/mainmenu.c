/**
 * @file mainmenu.c
 * @author John Wohlers (john@wohlershome.net)
 * @brief 
 * @version 0.1
 * @date 2022-03-03
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#include "mainmenu.h"


void mainMenuLoop(GameConfig *config, Player *players) {
    char keypress = '\0';
    bool valid_key;
    bool refresh_keys = true;
    short player_toggle = PLAYER_1;
    short len;
    short current_line = 0;

    config->gameMode.value = MODE_COMPUTER;
    drawMainMenu(config);
    drawMainMenuPlayerName(players, PLAYER_1);    

  	while (keypress != SMARTKEY_VI)
	{

        len = strlen(players[player_toggle].name);

        current_line = drawInstructions(current_line);

        if (refresh_keys == true) {
            // Set smart keys based on game mode
            if (config->gameMode.value == MODE_COMPUTER) {
                    smartkeys_display("\x1F\x1F\x1F   GAME\n\x1F\x1F   TYPE", NULL, NULL, NULL, "  GAME\n OPTIONS", "\x1F\x1F START\n  GAME");
            }
            else if (config->gameMode.value == MODE_LOCAL_TWO_PLAYER) {
                    smartkeys_display("\x1F\x1F\x1F   GAME\n\x1F\x1F   TYPE", " PLAYER\n  NAME", NULL, NULL, "  GAME\n OPTIONS",  "\x1F\x1F START\n  GAME");

            }
            else { // FUJI NET
                    smartkeys_display("\x1F\x1F\x1F   GAME\n\x1F\x1F   TYPE", NULL, NULL, NULL,"  GAME\n OPTIONS", "  PLAY\n\x1F\x1F\x1F ONLINE"); 
            }
            refresh_keys = false;
        }
        valid_key = true;
        keypress = eos_read_keyboard();
        if (keypress == SMARTKEY_I || keypress == KEY_TAB) {
            smartkeys_sound_play(SOUND_KEY_PRESS);
            toggleRadioOption(config->gameMode);
            refresh_keys = true;
            gotoxy(2,17);
            if (config->gameMode.value == MODE_LOCAL_TWO_PLAYER) {
                cprintf("PLAYER %d'S NAME", player_toggle + 1);
                drawMainMenuPlayerName(players, player_toggle);
            }
            else {
                cputs("YOUR NAME           ");
                drawMainMenuPlayerName(players, PLAYER_1);
            }
        }
        else if (keypress == SMARTKEY_II && config->gameMode.value == MODE_LOCAL_TWO_PLAYER) {
            smartkeys_sound_play(SOUND_KEY_PRESS);
            player_toggle = (player_toggle == PLAYER_1) ? PLAYER_2 : PLAYER_1;
            msx_color(INK_WHITE,INK_CYAN,INK_CYAN);
            gotoxy(2,17);
            cprintf("PLAYER %d'S NAME", player_toggle + 1);
            drawMainMenuPlayerName(players, player_toggle);
        }
		else if (((keypress >= 'a' && keypress <='z')||(keypress >= 'A' && keypress <='Z') || keypress == ' ' || keypress == '.' || keypress == '-') && len < 10) // Limit to valid keys
		{
			smartkeys_sound_play(SOUND_KEY_PRESS);
	        players[player_toggle].name[len] = keypress;
        	players[player_toggle].name[len+1] = '\0';
			drawMainMenuPlayerName(players, player_toggle);
		}
		else if (keypress == KEY_DELETE || keypress == KEY_BACKSPACE) // Backspace
		{
            smartkeys_sound_play(SOUND_TYPEWRITER_CLACK);
			if (len > 0) {
                players[player_toggle].name[len-1] = '\0';
                drawMainMenuPlayerName(players, player_toggle);
            }
        }
        else if (keypress == SMARTKEY_V) {
            smartkeys_sound_play(SOUND_MODE_CHANGE);
            optionsLoop(config);
            // Redraw main menu screen after return from options
            drawMainMenu(config);
            drawMainMenuPlayerName(players, player_toggle);
            refresh_keys = true;
        }
        else if (keypress == KEY_SHIFT || keypress == KEY_CAPS || keypress == SMARTKEY_VI) {
            //Nothing to do here, just don't buzz
            valid_key = true;
        }
        else if (keypress == KEY_DOWN_ARROW) {
            current_line++;
        }
        else if (keypress == KEY_UP_ARROW) {            
            if (current_line > 0) {
                current_line--;
            }
        }

        else {
            valid_key = false;
        }

        if (valid_key == false) {
            smartkeys_sound_play(SOUND_NEGATIVE_BUZZ);
        }
    }

    if (config->gameMode.value == MODE_COMPUTER) {
        strncpy(players[PLAYER_2].name, "ADAM", 5);
    }

    if (config->gameMode.value == MODE_FUJINET) {
        fujinetMenuLoop(config);
        fujinetSetupConnection(config, players);
    }

}