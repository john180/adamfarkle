/**
 * @file ui.c
 * @author John Wohlers (john@wohlershome.net)
 * @brief 
 * @version 0.1
 * @date 2022-03-01
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include "ui.h"


void initCheckOption(int x, int y, int boxColor, int textColor, char label[31], bool value, CheckOption *check_option) {
    check_option->x = x;
    check_option->y = y;
    check_option->boxColor = boxColor;
    check_option->textColor = textColor;
    strncpy(check_option->label, label, 31);
    check_option->isChecked = value;
}

void initRadioOptionGroup(int x, int y, int button_color, int text_color, int header_color, char label[31], int value, int num_options, int columns, char item_labels[6][31], char *pip_selected, char *pip_unselected, RadioOptionGroup *option_group) {
    int i;
    WidgetItem option;

    option_group->pip_selected = pip_selected;
    option_group->pip_unselected = pip_unselected;
       
    option_group->x = x;
    option_group->y = y;
    option_group->num_options = num_options;
    option_group->textColor = header_color;
    
    
    if (label != '\0') { 
        strncpy(option_group->label, label, 31);
    }

    for (i = 0; i < 3; i++) {
        if (option_group->label[0] != '\0') {
            initRadioOption(x + 1, y + 1 + i, button_color, text_color, '\0', &option);
        }
        else {
            initRadioOption(x, y + i, button_color, text_color, '\0', &option);            
        }
        option_group->options[i] = option;
        strncpy(option_group->options[i].label, item_labels[i], 31);
    }

    option_group->options[0].isSelected = true;


}

void initRadioOption(int x, int y, int button_color, int text_color, char label[31], WidgetItem *option) {
    option->x = x;
    option->y = y;
    option->textColor = text_color;
    option->isSelected = false;
    option->buttonColor = button_color;
    strncpy(option->label, label, 31);

}

void drawRadioOptionGroup(RadioOptionGroup *option_group){
    int i;
    char pip[2];

    if (option_group->label[0] != '\0') {
        gotoxy(option_group->x, option_group->y);
        msx_color(option_group->textColor, INK_CYAN,INK_CYAN);
        cprintf("%s", option_group->label);
    }

    for (i=0;i < option_group->num_options; i++) {
        gotoxy(option_group->options[i].x, option_group->options[i].y);
        msx_color(option_group->options[i].buttonColor, INK_CYAN, INK_CYAN);
        pip[0] = option_group->options[i].isSelected ? option_group->pip_selected : option_group->pip_unselected;
        pip[1] = '\0';
        cputs(pip);
        msx_color(option_group->options[i].textColor, INK_CYAN, INK_CYAN);
        cprintf(" %s", option_group->options[i].label);
    }
}

void drawCheckOption(CheckOption *check_option) {

    gotoxy(check_option->x, check_option->y);
    msx_color(check_option->boxColor, INK_CYAN, INK_CYAN);
    cputs (check_option->isChecked ? "\x9F" : "\x9e");
    msx_color(check_option->textColor, INK_CYAN, INK_CYAN);
    cprintf(" %s",check_option->label);

}

void toggleCheckOption(CheckOption *check_option) {
    
    check_option->isChecked = !check_option->isChecked;

    gotoxy(check_option->x, check_option->y);
    msx_color(check_option->boxColor, INK_CYAN, INK_CYAN);
    cputs (check_option->isChecked ? "\x9F" : "\x9e");

}

void toggleRadioOption_test(RadioOptionGroup *option_group) {
    char pip[2];
    int prev_value = option_group->value;
    int new_value = (option_group->value < option_group->num_options - 1) ? option_group->value + 1 : 0;  // If value is less than number of options - 1 (for 0 indexing)  increment, otherwise back to 0
    option_group->value = new_value;
    gotoxy(0,0);

#ifdef TEST_MODE    
    cprintf("index:%2d x:%2d y:%2d", prev_value, option_group->options[prev_value].x, option_group->options[prev_value].y);
#endif

}

void toggleRadioOption(RadioOptionGroup *option_group) {
    int prev_value = option_group->value;
    int new_value = (option_group->value < option_group->num_options - 1) ? option_group->value + 1 : 0;  // If value is less than number of options - 1 (for 0 indexing)  increment, otherwise back to 0
    option_group->value = new_value;

    msx_color(option_group->options[prev_value].buttonColor,INK_CYAN,INK_CYAN);

    // unset previous radio
    option_group->options[prev_value].isSelected = false;
    gotoxy(option_group->options[prev_value].x, option_group->options[prev_value].y);
    cprintf("%c", option_group->pip_unselected);

    gotoxy(0,0);
#ifdef TEST_MODE    
    cprintf("index:%2d x:%2d y:%2d", prev_value, option_group->options[prev_value].x, option_group->options[prev_value].y);
#endif

    option_group->options[new_value].isSelected = true;
    gotoxy(option_group->options[new_value].x, option_group->options[new_value].y);
    cprintf("%c", option_group->pip_selected);

}

void disableField(int x, int y, int len) {
    gotoxy(x,y); 
    msx_color(INK_GRAY,BOARD_COLOR,BORDER_COLOR);                
    for (int i=0;i<len;i++) {
       cputs ("\x9A");
    }
}


