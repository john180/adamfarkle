/**
 * @file dice.h
 * @author John Wohlers (john@wohlershome.net)
 * @brief 
 * @version 0.1
 * @date 2022-02-22
 * 
 * @copyright Copyright (c) 2022
 * 
 */


#ifndef DICE_H
#define DICE_H

#include <msx.h>
#include <stdbool.h>
#include <sys/ioctl.h>
#include <conio.h>
#include <stdlib.h>
#include "util.h"
#include "sound.h"

typedef struct Die
{
    int value;
    int timesRolled;
    bool saved;
    bool locked;
    bool visible;
    int x;
    int y;
    int color;
} Die;

typedef struct Roll
{
    Die dice[6];
    short counts[6];
    short roll_counter;
} Roll;


void drawDie(int x, int y, int val, int color);

int placeDieOnBoard(int col, Die *dieObj);

void saveDie(Die *dieObj);

void releaseDie( Die *dieObj);

void removeDie( Die *dieObj);

void rollDie( Die *dieObj);

void resetDie( Die *dieObj);

bool isDieSaved( Die *dieObj);

bool isDieLocked(Die *dieObj);

bool isAllLocked(Die *dice_group);

void toggleDieSaveState( Die *dieObj);

void initRoll(Roll *myroll);

/**
 * @brief returns a count of the nuber of banked dice
 * 
 */
short countBankedDice(Die *dice_group);

#endif /* DICE_H */