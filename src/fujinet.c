/**
 * @file fujinet.c
 * @author John Wohlers (john@wohlershome.net)
 * @brief Functions for fujinet game play
 * @version 0.1
 * @date 2022-02-15
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include "fujinet.h"


#define CONNECTED 2   // Bit 1 of returned status is whether we are connected.

#define RETRY_COUNT 128 // # of times to retry

#define READ_WRITE  12  // protocol channel needs both read and write
#define NONE         0  // Translation mode = none

unsigned char response[1024]; // Global response buffer used by recv()

/**
 * @brief Check for presense of fujinet
 * 
 * colem Emulator returned 0x00 
 * personal ADAM returned 0x80 when fujinet present
 * 
 * @return true 
 * @return false 
 */
bool isFujiNet(void) {
	DCB *dcb = eos_find_dcb(FUJINET_DEVICE);
	unsigned char status;

	eos_scan_for_devices();

	status = eos_request_device_status(FUJINET_DEVICE,dcb);

	return ((status < 0x80) ? false : true );
  
}

// FUJINET FUNCTIONS /////////////////////////////////////////////////////////
//     Taken from Thom Cherryhomes Connect Four Game

/**
 * Connect to host, wait for connection until timeout.
 * @param h char pointer to host name
 * @return true if connected.
 */
bool fujinetConnect(char *h)
{
  unsigned char retries=RETRY_COUNT;
  DCB *dcb = eos_find_dcb(NET);
  
  struct
  {
    char cmd;
    char mode;
    char translation;
    char url[96];
  } c;

  c.cmd = 'O';
  c.mode = READ_WRITE;
  c.translation = NONE;
  sprintf(c.url,"N:TCP://%s:6502/",h);

  eos_write_character_device(NET,(char *)c, sizeof(c));

  while (retries > 0)
    {
      csleep(5);
      
      while (eos_request_device_status(NET,dcb) < ACK); // sit and spin; Wait for status

      if (eos_get_device_status(NET) & CONNECTED) // Are we connected?
	return true;
      else
	retries--;      
    }

  return false; // Unable to connect.  
}

/**
 * @brief ask FujiNet to wait for a TCP connection on port 6502
 */
void fujinetListen(void)
{
  struct
  {
    char cmd;
    char mode;
    char translation;
    char url[16];
  } listenCmd;

  listenCmd.cmd = 'O';
  listenCmd.mode = READ_WRITE;
  listenCmd.translation = 0;
  strcpy(listenCmd.url,"N:TCP://:6502/");

  eos_write_character_device(NET,(char *)listenCmd,sizeof(listenCmd));
}

/**
 * @brief is there a connection waiting?
 * @return true if connection waiting.
 */
bool fujinetConnectionWaiting(void)
{
  DCB *dcb = eos_find_dcb(NET);
  char c;
  while (eos_request_device_status(NET,dcb) < 0x80);
  c=eos_get_device_status(NET);

  if (c==2)
    return true;
  else
    return false;
}

/**
 * @brief Accept connection
 */
void fujinetAcceptConnection(void)
{
  struct
  {
    char cmd;
    char aux1;
    char aux2;
  } a;

  a.cmd = 'A';
  a.aux1 = READ_WRITE;
  a.aux2 = NONE;
  eos_write_character_device(NET,(char *)a,sizeof(a));
}

/**
 * @brief Close connection
 */
void fujinetCloseConnection(void)
{
  struct
  {
    char cmd;
    char aux1;
    char aux2;
  } a;

  a.cmd = 'C';
  a.aux1 = NONE;
  a.aux2 = NONE;
  eos_write_character_device(NET,(char *)a,sizeof(a));
}

/**
 * @brief Send data to other host
 * @param buf The buffer to send
 */
void fujinetSend(char *buf)
{
  struct
  {
    char cmd;
    char buf[64];
  } s;

  s.cmd = 'W';

  strcpy(s.buf,buf);
  strcat(s.buf,"\n"); // Terminate with LF

  eos_write_character_device(NET,(char *)s,strlen(s.buf)+1); // To account for command byte
}

/**
 * @brief receive data from other host
 * @param buf The receiving buffer.
 */
void fujinetRecv(char *buf)
{
  char *p=NULL;
  DCB *dcb = eos_find_dcb(NET);
  
  while (eos_read_character_device(NET,response,sizeof(response)) != ACK); // We have to always ask for receive in 1024 bytes.

  strncpy(buf,response,dcb->len);

  p = strchr(buf,'\n');

  if (p!=NULL)
    *p=0; // remove EOL
}

/**
 * @brief Listen for connection on TCP port 6502 (server mode)
 */
bool fujinetListenForConnection(void)
{
  fujinetListen();

  status("  WAITING FOR CONNECTION TO TCP PORT 6502...");

  while (fujinetConnectionWaiting()==false);

  status("  CONNECTION WAITING...ACCEPTING...");

  fujinetAcceptConnection();
  
  return true;
}

/**
 * @brief connect to host pointed by n
 * @param n pointer to hostname
 * @return false = successful (as listen will be false)
 */
bool fujinetConnectToHost(char *n)
{
  bool connection_status;
  
  status("  CONNECTING TO HOST, PLEASE WAIT...");
  if (fujinetConnect(n))
    {
      status("  CONNECTION SUCCESSFUL.");
      connection_status=true;
    }
  else
    {
      status("  COULD NOT CONNECT.");
      connection_status=false;
    }
  
  sleep(1);  
  return connection_status;
}

/**
 * @brief Return a random number from the FujiNet Device.
 * 
 * @return unsigned long 
 */
unsigned long fujinetRandomNumber(void)
{
  char r=0xD3;  // Random number request code
  unsigned long *p=(unsigned long *)&response[0];  //unsigned long on ADAM is same byte length as int on esp32
  
  eos_write_character_device(FUJINET_DEVICE,&r,1); // Send request to FujiNet
  eos_read_character_device(FUJINET_DEVICE,response,1024); // Read response

  return *p;
}

unsigned char fujinetAppkeyRead(unsigned int creator, unsigned char app, unsigned char key, char *buf)
{
  unsigned char r=0;
  
  struct
  {
    unsigned char cmd;
    unsigned short creator;
    unsigned char app;
    unsigned char key;
  } a;

  a.cmd = ADAMNET_SEND_APPKEY_READ;
  a.creator = creator;
  a.app = app;
  a.key = key;

  r=eos_write_character_device(FUJINET_DEVICE,(unsigned char *)a,sizeof(a));
  if (r > 0x80)
    return r;
  else
    return eos_read_character_device(FUJINET_DEVICE,buf,1024);
}

unsigned char fujinetAppkeyWrite(unsigned int creator, unsigned char app, unsigned char key, char *buf)
{
  struct
  {
    unsigned char cmd;
    unsigned short creator;
    unsigned char app;
    unsigned char key;
    char data[64];
  } a;

  a.cmd = ADAMNET_SEND_APPKEY_WRITE;
  a.creator = creator;
  a.app = app;
  a.key = key;
  strncpy(a.data,buf,sizeof(a.data));

  return eos_write_character_device(FUJINET_DEVICE,(unsigned char *)a,sizeof(a));
}


void fujinetMenuLoop(GameConfig *config) {
  unsigned char pip_selected = '\xAF';
  unsigned char pip_unselected = '\x20';
  char keypress = '\0';
  bool valid_key;

  char mode_labels[2][31] = {{"HOST"}, {"GUEST"}};
  initRadioOptionGroup(2,11,INK_WHITE,INK_BLACK,INK_DARK_BLUE,"\0", 0, 2, 1, mode_labels, pip_selected, pip_unselected, config->fujiNetMode);

  drawFujiOpponentScreen(config);
  smartkeys_display("    MODE", NULL, NULL, NULL, NULL,"\x1F\x1F START\n  GAME");

	while (keypress != SMARTKEY_VI)
	{
      int len = strlen(config->fujiNetHostName);

      // Update display
      if (config->fujiNetMode.value == FUJINET_MODE_GUEST) { //Enable Host name editing and fill in current host name
        gotoxy(2,17); // Field Width 28
        msx_color(INK_BLACK,BOARD_COLOR,BORDER_COLOR);                
        cprintf( "%-28s", lastNchar(config->fujiNetHostName,28));
      }
      else {
        disableField(2,17,28);
      }



      keypress = eos_read_keyboard();
      
      valid_key = true; // true unless we fall out to default on switch, meaning invalid input


      if (keypress == KEY_TAB || keypress == SMARTKEY_I) 
      {
              toggleRadioOption(config->fujiNetMode);
              smartkeys_sound_play(SOUND_KEY_PRESS);
      }
      else if (keypress == SMARTKEY_VI && config->fujiNetHostName[0] == '\0' && config->fujiNetMode.value == FUJINET_MODE_GUEST ) {
        smartkeys_sound_play(SOUND_NEGATIVE_BUZZ);
        keypress = '\0';
        status("  ENTER A VALID HOSTNAME OR IP ADDRESS");
        sleep(5);
        smartkeys_display("    MODE", NULL, NULL, NULL, NULL,"\x1F\x1F START\n  GAME");
      }
      else if (keypress == SMARTKEY_VI) {
        // Just here so the key is valid...
      }
      else if (config->fujiNetMode.value == FUJINET_MODE_GUEST && ((keypress >= '0' && keypress <='9') || (keypress >= 'a' && keypress <='z')||(keypress >= 'A' && keypress <='Z') || keypress == ' ' || keypress == '.' || keypress == '-') )
      {
      /**
       * Valid Characters per RFC1034    
       *   --Technically there should be better validation here... 
       *   --According to RFC 1035 the length of a FQDN is limited to 255 characters, and each label can be no longer than 63 bytes.
       **/
          smartkeys_sound_play(SOUND_KEY_PRESS);
          config->fujiNetHostName[len] = keypress;
          config->fujiNetHostName[len+1] = '\0';

      }
      else if (config->fujiNetMode.value == FUJINET_MODE_GUEST && (keypress == KEY_DELETE || keypress == KEY_BACKSPACE))
      {
          smartkeys_sound_play(SOUND_TYPEWRITER_CLACK);
          if (len > 0) {
              config->fujiNetHostName[len-1] = '\0';
          }
          else {
            valid_key = false;
          }
      }
      else {
              valid_key = false;              
      }


      if (valid_key == false) {
          smartkeys_sound_play(SOUND_NEGATIVE_BUZZ);
      }
  }

}

/***********************************************************************************************
 * GAME SPECIFIC CODE BELOW
 * 
 */


void fujinetSetupConnection(GameConfig *config, Player *players) {
  bool isConnected;

  if (config->fujiNetMode.value == FUJINET_MODE_HOST) {
    isConnected = fujinetListenForConnection();  // Host is player 1
  }
  else {
    //TODO: Guest Mode Code
    //TODO: Set current player to player 2 - Need to move the indicator into one of the structs
  }
	fujinetSend(players[PLAYER_1].name);
  fujinetRecv(players[PLAYER_2].name);

}

/**
 * @brief Send the current roll, and the state of the Bank (for good measure)
 * 
 * @param dice_group 
 */
void fujinetSendDice(Die *dice_group) {
  char dice_state[21];
  dice_state[0] = 'R'; // Message Prefix to indicate this message is a dice roll
  dice_state[20] = 0;  // null terminate for good measure

  for (int i=0; i < 6; i++) {
    dice_state[i + 7] = dice_group[i].y + 65;
    if (dice_group[i].saved == true ) {
      dice_state[i + 1] = '0';
      dice_state[i + 13] = dice_group[i].value + '0';
    }
    else {
      dice_state[i + 13 ] = '0';
      dice_state[i + 1] = dice_group[i].value + '0';
    }
  }

  fujinetSend(dice_state);
}

/**
 * @brief 
 * 
 * @param keycode 
 */
void fujinetSendKey(unsigned char keycode) {
  char message[3];
  message[0] = 'K'; // Message Prefix to indicate this message is a key press
  message[2] = 0;   // null terminate for good measure

  message[1] = '_';
  //debug(keycode);  
  /***
   *  Convert special keys to ascii values for simpler emulation of remote clinet (yeah, i know overhead)
   *  nc 192.168.10.169 6502
   */
 

  if (keycode >= SMARTKEY_I && keycode <= SMARTKEY_VI) {    
    message[1] = (keycode - 64);  
  }
  if (keycode == 9) {    // Tab
    message[1] = 'T';
  }
  if (keycode == 13) {    // Return
    message[1] = 'R';
  }
  if (keycode == 20) {    // Space
    message[1] = 'S';
  }

  // Only send valid key presses
  if (message[1] != '_') {
    fujinetSend(message); 
  }
}

void fujinetSendAnimation(int animation) {
  char message[3];
  message[0] = 'A'; // Message Prefix to indicate this message is an animation
  message[1] = animation + '0';
  message[2] = 0;   // null terminate for good measure

  fujinetSend(message); 

}

/**
 * @brief Sends the scores
 * 
 * @param players 
 * @param turn
 */
void fujinetSendScore(Player players[MAX_PLAYERS], int turn) {
  char message[10];

  sprintf(message, "S%d%d", 0, players[0].score);
  fujinetSend(message); 

  sprintf(message, "S%d%d", 1, players[1].score);
  fujinetSend(message); 

  sprintf(message, "S%d%d", 9, turn);
  fujinetSend(message); 
}

void fujinetSendTaunt(int taunt) {
  char message[3];
  message[0] = 'T'; // Message Prefix to indicate this message is a taunt
  message[1] = taunt + '0';
  message[2] = 0;   // null terminate for good measure

  fujinetSend(message); 

}