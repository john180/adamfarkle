/**
 * @file brain.c
 * @author John Wohlers (john@wohlershome.net)
 * @brief 
 * @version 0.1
 * @date 2022-03-06
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include "brain.h"

 char witty_arr[WITTY_MAX][46] = { //46 char plus null
       //123456789012345678901234567890123456789012345
        "This will be my best turn yet!",
        "Watch this!",
        "Leeeeerooy Jenkins!",
        "How do I blow on the dice?",
        "No whammy! No whammy! Stop! Oops wrong game",
        "How about a nice game of chess?"
};

/**
 * @brief Initialize the key stack struct, empty the key positons, and set the stack count to 0;
 * 
 * @param key_stack 
 */
void initKeyStack(KeyStack *key_stack) {
    for (int i = 0; i < 6; i++) {
        key_stack->keys[i] = '\0';
    }

    key_stack->stackCount = 0;
}

/**
 * @brief Push a key onto the stack and increment the counter
 * 
 * @param key 
 * @param key_stack 
 */
void pushKey(char key, KeyStack *key_stack) {
    key_stack->keys[ key_stack->stackCount ] = key;
    key_stack->stackCount++;
}

/**
 * @brief Pop a key from off the stack and reduce the stack count
 * 
 * @param key_stack 
 * @return char 
 */
char popKey(KeyStack *key_stack) {
    char key;

    key = key_stack->keys[ key_stack->stackCount - 1 ]; // -1 to cope with zero index
    key_stack->keys[ key_stack->stackCount -1 ] = '\0';

    key_stack->stackCount--;

    return(key);
}

/**
 * @brief 
 * 
 * TODO: Convert this to use the key stack
 * @param dice_group 
 * @return bool 
 */
char saveOnes(Die *dice_group, KeyStack *key_stack) {
    short j,i = 0;
    bool retval = false;

    for (i=0; i < 6; i++) {
        j = dice_group[i].value;
        if (j == 1 && dice_group[i].locked == false && dice_group[i].saved == false) {
            pushKey(SMARTKEY_I + i, key_stack);
            retval = true;
        }
    }
    return retval;
}

/**
 * @brief 
 * 
 * TODO: Convert this to use the key stack
 * @param dice_group 
 * @return char 
 */
char saveFives(Die *dice_group) {
    short j,i = 0;

    for (i=0; i < 6; i++) {        
        j = dice_group[i].value;
        if (j == 5 && dice_group[i].locked == false && dice_group[i].saved == false) {
            return SMARTKEY_I + i;
        }
    }
    return false;
}

/**
 * @brief Are there sets on the board? if so update the key_stack 
 * 
 * TODO: Add code to only save sets of twos if they are >3 or there are no 1 or 5s...  Check for saved=true & locked = false and set flag check flag on later loop
 * @param dice_group 
 * @param stack 
 * @return true 
 * @return false 
 */
bool saveSets(Die *dice_group, KeyStack *key_stack) {
    short count[6] = {0,0,0,0,0,0};
    short i;
    bool result = false;

    // Count the die
    for (i=0; i < 6; i++) {
        if (dice_group[i].saved == false) {  // we only care about this roll... previously saved die don't count
            int val = dice_group[i].value;
            count[val - 1]++;
        }      
    }

    for (i=0; i < 6; i++) {
        //Hey ADAM don't save a set of 3 2's if you don't have to... (If theres a 1 or a 5, and we have exactly 3 twos, and we are about to process 2's move along to 3's)
        if ((count[0] > 0 || count[4] > 0) && count[1] == 3 && i == 1) {            
            i++;
        }
        if (count[i] >= 3) {
            if (i == 5 ) {
                farkle_sound_play(SOUND_COMMENT);
                status("\n  ADAM: A set of sixes! Am I in sleep mode?");
            }
            // We have a set, cycle through the dice to add thier keystroke to the stack
            for (int dice_col=0; dice_col<6; dice_col++) {
                if (dice_group[ dice_col ].saved == false && dice_group[ dice_col ].locked == false && dice_group[ dice_col ].value == i+1) {

                    pushKey(SMARTKEY_I + dice_col, key_stack);
                    result = true; // indicate there is something to save                    
                }
            }
        }
    }

    return (result);
}

/**
 * @brief Logic to let ADAM save 3 pair if it occurs.
 * 
 * @param dice_group 
 * @param key_stack 
 * @return true 
 * @return false 
 */
bool saveThreePair(Die *dice_group, KeyStack *key_stack) {
    short count[6] = {0,0,0,0,0,0};
    short i,j;
    bool result = false;
    short pairs_count = 0;

    for (i=0; i < 6; i++) {
        j = dice_group[i].value;
        if (j > 0 && dice_group[i].locked == false && dice_group[i].saved == false) {
            count[j - 1]++;
        }
    }

    // check for 3 pairs
    for (i=0; i < 6; i++) {  
        if (count[i] == 6) { 
            pairs_count = 3;
            break; // no point in continuing counting we have 6 of a kind!
        }
        if (count[i] >= 4 && count[i] < 6) {
            pairs_count = pairs_count + 2;
        }
        if (count[i] >= 2 && count[i] < 4) {
            pairs_count++;
        }
    }
    if (pairs_count == 3 ) {
        farkle_sound_play(SOUND_COMMENT);
        status("\n  ADAM: WOW! 3 pair! I must have lucky RAM!");
        for (i=0; i<6; i++) {
            pushKey(SMARTKEY_I + i, key_stack);

        }
        result = true;
    }

    return(result);

}

/**
 * @brief Checks for, and saves a straight to the key stack.
 * 
 * @return true 
 * @return false 
 */
bool saveStraight(Die *dice_group, KeyStack *key_stack){
    bool valid = true;
    bool straight[6] = {false,false,false,false,false,false};
    int i, j = 0;

    for (i=0; i < 6; i++) {
        
        if (dice_group[i].saved == false && dice_group[i].locked == false) {  // we only care about this roll... previously locked die were already tallied           
            int val = dice_group[i].value;
            straight[val-1] = true; 
        }
    }
    for (i=0; i < 6; i++) {  // check for a straight
        if (straight[i] == false) valid = false;
    }
    if (valid == true) {
        farkle_sound_play(SOUND_COMMENT);
        status("\n  ADAM: 1 2 3 4 5 6, A straight!");
        for (i=0; i<6; i++) {
            pushKey(SMARTKEY_I + i, key_stack);
        }        
    }
    return (valid);
}

/**
 * @brief Adam can make comments
 * 
 */
void sayComment(void) {
    short comment;
    char buffer[100];

    comment = (rand() % WITTY_MAX);
    sprintf(buffer, "\n  ADAM: %s", witty_arr[comment]);
    farkle_sound_play(SOUND_COMMENT);
    status( buffer );
}
