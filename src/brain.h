/**
 * @file brain.h
 * @author John Wohlers (john@wohlershome.net)
 * @brief Logic for ADAM opponent. 
 * @version 0.1
 * @date 2022-03-06
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef BRAIN_H
#define BRAIN_H

#include "sound.h"
#include <stdlib.h>
#include <stdbool.h>
#include <msx.h>
#include <sys/ioctl.h>
#include <conio.h>
#include <smartkeys.h>
#include <eos.h>
#include "util.h"
#include "adamkeys.h"
#include "dice.h"
#include "screens.h"
#include "scoreboard.h"
#include "player.h"
#include "config.h"
#include "gamelogic.h"


#define WITTY_MAX 6


typedef struct KeyStack  
{
    int stackCount;
    unsigned char keys[6];
} KeyStack;


void initKeyStack(KeyStack *key_stack);

void pushKey(char key, KeyStack *key_stack);

char popKey(KeyStack *key_stack);


/**
 * @brief 
 * 
 * @param dice_group 
 * @param key_stack 
 * @return char 
 */
char saveOnes(Die *dice_group, KeyStack *key_stack);

/**
 * @brief 
 * 
 * @param dice_group 
 * @return bool 
 */
char saveFives(Die *dice_group);

bool saveSets(Die *dice_group, KeyStack *key_stack);

bool saveThreePair(Die *dice_group, KeyStack *key_stack);

bool saveStraight(Die *dice_group, KeyStack *key_stack);

void sayComment(void);

#endif