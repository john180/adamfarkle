/**
 * @file player.c
 * @author John Wohlers (john@wohlershome.net)
 * @brief 
 * @version 0.1
 * @date 2022-02-26
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include "player.h"

void initPlayers(Player players[MAX_PLAYERS]) {
    int i;

    for (i=0;i < MAX_PLAYERS; i++) {
        sprintf(players[i].name, "Player %d", i+1);
        players[i].id = i;
        players[i].number = i+1;        
        players[i].score = 0;
        players[i].farkle_count = 0;
        players[i].initialFiveHundred = false;
    }

}