/**
 * @file player.h
 * @author John Wohlers (john@wohlershome.net)
 * @brief 
 * @version 0.1
 * @date 2022-02-26
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef PLAYER_H
#define PLAYER_H

#include <msx.h>
#include <sys/ioctl.h>
#include <conio.h>
#include <stdlib.h>
#include <stdbool.h>


#define MAX_PLAYERS 2

typedef struct Player
{
    int id; // Thinking ahead to internet play... make it large enough to hold an ID from the server, but not rediculiously large (GUID)
    int number;
    int score;
    char name[15];
    int farkle_count;  
    bool initialFiveHundred;
} Player;

/**
 * @brief 
 * 
 * @param players 
 */
void initPlayers(Player players[MAX_PLAYERS]);

#endif