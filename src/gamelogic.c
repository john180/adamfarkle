/**
 * @file gamelogic.c
 * @author John Wohlers (john@wohlershome.net)
 * @brief 
 * @version 0.1
 * @date 2022-02-22
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include "gamelogic.h"

/**
 * @brief Checks to make sure the player banked at least one die this turn.  
 *        prevents re-rolling w/o banking anything.
 * 
 * @param dice_group 
 * @param valid_move 
 */
bool checkBanked(Die *dice_group, ValidMove *valid_move) {
    int i;

    valid_move->valid=false;
    strncpy(valid_move->reason, "\n  You must bank at least one die per turn.", 60);
    for (i=0; i < 6; i++) {
        if (dice_group[i].locked == false && dice_group[i].saved == true) {
            valid_move->valid=true;
        }
    }

}


void checkToxic(Die *dice_group, ValidMove *valid_move) {
    short count = 0;
    short i,j;

    valid_move->valid=true;

    for (i=0; i < 6; i++) {
        j = dice_group[i].value;
        if (j == 2 && dice_group[i].locked == false && dice_group[i].saved == false) {
            count++;
        }        

    }

    if (count >= 4) {  // We have toxic Twos...
        valid_move->valid = false;
        strncpy(valid_move->reason, "\n  Toxic Twos!", 60);
    }
}

/**
 * @brief Check the die in the passed bin Return true if a valid die or set of die exist, false otherwise
 *        false can be used to reject the die in the keep bin
 * @param dice_group the array of dice values to check
 * @param checkBanked if true, check the newly banked dice, if false, check the rolled dice
 * @param valid_move structure containing move validity and error message
 */
void checkValid(Die *dice_group, bool checkBanked, ValidMove *valid_move) {

    valid_move->valid=true;

    bool straight[6] = {false,false,false,false,false,false};
    bool straight_flag = true;
    short pairs_count = 0;
    int count[6] = {0,0,0,0,0,0};


    int i, j = 0;

    for (i=0; i < 6; i++) {
        j = dice_group[i].value;
        if (j > 0 && dice_group[i].locked == false && dice_group[i].saved == checkBanked) {
            straight[j - 1] = true;
            count[j - 1]++;
        }
    }

    for (i=0; i < 6; i++) {  // check for a straight 
        if (straight[i] == false) {
            straight_flag = false;
            valid_move->valid=false;
        }
    }

    // check for 3 pairs
    for (i=0; i < 6; i++) {  
        if (count[i] == 6) { 
            pairs_count = 3;
            break; // no point in continuing counting we have 6 of a kind!
        }
        if (count[i] >= 4 && count[i] < 6) {
            pairs_count = pairs_count + 2;
        }
        if (count[i] >= 2 && count[i] < 4) {
            pairs_count++;
        }
    }
    if (pairs_count == 3 ) {
        valid_move->valid=true;
        // Move is valid, no need to continue checking;
        return;
    }

    // check for 1 or 5 or triplets or better
    if ( count[0] > 0 || count[4] > 0 ||  count[1] >= 3 || count[2] >= 3  || count[3] >= 3 || count[5] >= 3 ) {
        valid_move->valid = true;
    }
    else {
        strncpy(valid_move->reason, "\n  Farkle!", 60);
    }

    if (straight_flag == true ) {
        valid_move->valid = true;
    }
    else {
        if (checkBanked == true ) { // Only perform these checks on the bank, not on the board
            if ( (count[1] > 0 && count[1] < 3) || (count[2] > 0 && count[2] < 3)  || (count[3] > 0 && count[3] < 3) || (count[5] > 0 && count[5] < 3) ) {
                valid_move->valid = false;            
                strncpy(valid_move->reason, "\n  Please check your banked dice and try again.", 60);
            }

            if ( count[0] == 0 && count[1] == 0 && count[2] == 0 && count[3] == 0 && count[4] == 0 && count[5] == 0 ) {
                valid_move->valid = true;
                valid_move->reason[0] = NULL;
            }
        }
    }
}


/**
 * @brief Main game loop
 * 
 * @param players
 * @param config
 * 
 */
void gameLoop(Player *players, GameConfig *config) {

    int gameover = false;
    char message[60];
    char remote_message[20];
    short current_player = PLAYER_1; 
    int turn_mode = TURN_MODE_ROLL;
    unsigned char keypress;
    bool valid_key = true;
    ValidMove valid_turn; 
    KeyStack new_key_stack;
    int current_turn_score = 0;
    int i;
    int stack_loop;
    unsigned char adam_key_stack[6] = {1,0,0,0,0,0};    

    Roll current_dice_roll;

    initRoll(current_dice_roll);
    initKeyStack(new_key_stack);

    for (i=0; i<6; i++) {
        resetDie(current_dice_roll.dice[i]); // Initialize the die
    }

    if (config->gameMode.value == MODE_FUJINET && current_player == PLAYER_1) fujinetSendScore(players, current_turn_score);
    drawCurrentPlayer(players, current_player);

    while(gameover == false) { 

        if (config->gameMode.value == MODE_COMPUTER && current_player == PLAYER_2) {            
            sleep(1);
            if (current_dice_roll.roll_counter == 0) {
                sayComment();

                sleep(1);
                keypress = 0xF0;
                turn_mode == TURN_MODE_ROLL;
            }
            else {
                if (new_key_stack.stackCount == 0) {
                    saveStraight(current_dice_roll.dice, new_key_stack);
                }
                if (new_key_stack.stackCount == 0) {
                    saveThreePair(current_dice_roll.dice, new_key_stack);
                }
                if (new_key_stack.stackCount == 0) {
                    saveSets(current_dice_roll.dice, new_key_stack);                      
                }
                if (new_key_stack.stackCount == 0) {
                    saveOnes(current_dice_roll.dice, new_key_stack);
                }                    
                if (new_key_stack.stackCount == 0) {
                        keypress = saveFives(current_dice_roll.dice);
                        if (keypress == false) {
                            turn_mode = TURN_MODE_ROLL;

                            // TODO: Need some smarter checks here.
                            if (current_turn_score + tallyRollScore(current_dice_roll.dice, current_dice_roll.roll_counter, config) >= 500 && countBankedDice(current_dice_roll.dice) < 6) {
                                turn_mode = TURN_MODE_PASS;
                            }
                            keypress = 0xF0;
                        }                   
                }
                else {
                    keypress = popKey(new_key_stack); //move the key to the input                   
                    turn_mode == TURN_MODE_ROLL;                    
                }                

            }
            menuTurnOption(turn_mode);
        }
        else if (config->gameMode.value == MODE_FUJINET && current_player == PLAYER_2) {
            //TODO: handle remote commands
            fujinetRecv(remote_message);
//DEBUGING
            gotoxy(8,0);
            cprintf("Message: %s",remote_message);
//  
            switch(remote_message[0])
            {
                case 'A':  // Animations
                    doAnimation(remote_message[1] - '0');
                    keypress = 0xF1;
                    break;

                case 'C': // Commands
                    if (remote_message[1] == 'T') { // Switch Player                    
                        current_player = remote_message[2];
                    }
                    break;

                case 'K': // Keypresses
                    if (remote_message[1] >= 'A' && remote_message[1] <= 'F') {    
                        keypress = (remote_message[1] + 64);  //translate to smartkeys
                    }
                    if (remote_message[1] == 'T') {    // Tab
                        keypress = KEY_TAB;
                    }
                    if (remote_message[1] == 'R') {    // Return - TODO: use bypass code once we get further along
                        keypress = KEY_RETURN;
                    }
                    if (remote_message[1] == 'S') {    // Space - TODO: use bypass code once we get further along
                        keypress = KEY_SPACE;
                    }
                    break;

                case 'R': // Display Roll
                    for (int i=0; i<6; i++) {
                        
                        if (remote_message[i+1] > 0) {
                            current_dice_roll.dice[i].y = remote_message[i+7] - 65;  //set dice position
                            current_dice_roll.dice[i].value = remote_message[i+1] - '0'; //set dice value
                            placeDieOnBoard(i+1, current_dice_roll.dice[i]); 
                        }

                    }
                    keypress = 0xF1; // Bypass local rolling, etc...
                    break;

                case 'S': // Score Value
                    if (remote_message[1] - '0' == PLAYER_1) {
                        players[PLAYER_1].score = subInt(remote_message);
                    }
                    if (remote_message[1] - '0' == PLAYER_2) {
                        players[PLAYER_2].score = subInt(remote_message);
                    }
                    if (remote_message[1] - '0' == 9) {
                        current_turn_score = subInt(remote_message);
                    }
                    updateScoreDisplay(players, current_turn_score);
                    keypress = 0xF1; // Bypass local rolling, etc...
                    break;
            }

            //Parse the message received to figure out what to do
            
        }
        else {
            //keypress = cgetc();
            keypress = eos_read_keyboard();
        }

        valid_key = true; // true unless we fall out to default on switch, meaning invalid input
        valid_turn.valid = true;
        
        // If we are in online mode, and it's our turn send the keypress to the opponent 
        if (config->gameMode.value == MODE_FUJINET && current_player == PLAYER_1) fujinetSendKey(keypress);

        switch(keypress)
        {
            case SMARTKEY_I:
                toggleDieSaveState(current_dice_roll.dice[0]);                
                break;
            case SMARTKEY_II:
                toggleDieSaveState(current_dice_roll.dice[1]);
                break;
            case SMARTKEY_III:
                toggleDieSaveState(current_dice_roll.dice[2]);
                break;
            case SMARTKEY_IV:
                toggleDieSaveState(current_dice_roll.dice[3]);
                break;
            case SMARTKEY_V:
                toggleDieSaveState(current_dice_roll.dice[4]);            
                break;
            case SMARTKEY_VI:
                toggleDieSaveState(current_dice_roll.dice[5]);            
                break;
            case KEY_WILD_CARD:
                fujinetSendTaunt(1);
                break;
#ifdef TEST_MODE
            case KEY_2:
                animateToxicTwos();
                break;
            case KEY_3:
                players[current_player].score = players[current_player].score + 1000;
                break;
            case KEY_4:
                status("STRAIGHT TEST");
                for (int d=0; d<6; d++ ) {
                    current_dice_roll.dice[d].value = d+1;
                    placeDieOnBoard(d+1, current_dice_roll.dice[d]);
                }
                break;
            case KEY_5:
                status ("3 Farkle Animation TEST");
                drawPlayField();
                animateThreeFarkle();
                break;

            case KEY_6:
                status ("  3 Pairs Test");
                    current_dice_roll.dice[0].value = 2;
                    placeDieOnBoard(1, current_dice_roll.dice[0]);
                    current_dice_roll.dice[1].value = 4;
                    placeDieOnBoard(2, current_dice_roll.dice[1]);
                    current_dice_roll.dice[2].value = 1;
                    placeDieOnBoard(3, current_dice_roll.dice[2]);
                    current_dice_roll.dice[3].value = 1;
                    placeDieOnBoard(4, current_dice_roll.dice[3]);
                    current_dice_roll.dice[4].value = 4;
                    placeDieOnBoard(5, current_dice_roll.dice[4]);
                    current_dice_roll.dice[5].value = 2;
                    placeDieOnBoard(6, current_dice_roll.dice[5]);


                break;
#endif

            case KEY_TAB:
                smartkeys_sound_play(SOUND_KEY_PRESS);
                turn_mode = (turn_mode == TURN_MODE_PASS ? TURN_MODE_ROLL : TURN_MODE_PASS); // alternate between turn mode states
                menuTurnOption(turn_mode);
                break;
            case 0xF0: // Computer would like to roll or pass
            case KEY_RETURN:    
            case KEY_SPACE:            

                    //TODO: Bypass Roll if online opponent this will be handled by remote messages

                    if (current_dice_roll.roll_counter > 0 ) { // Have any dice been banked this turn?
                        checkBanked(current_dice_roll.dice, &valid_turn);
                        if (valid_turn.valid == true) {
                            checkValid(current_dice_roll.dice, true, &valid_turn); // Checks the banked dice for validity
                        }
                    }

                    if (valid_turn.valid == false) { // Invalid die...
                        status(valid_turn.reason);
                    } else 
                    { // Valid Bank 
                        if (turn_mode == TURN_MODE_ROLL) {
                            current_turn_score = current_turn_score + tallyRollScore(current_dice_roll.dice, current_dice_roll.roll_counter, config);                            
                            if (config->gameMode.value == MODE_FUJINET && current_player == PLAYER_1) fujinetSendScore(players, current_turn_score);
                            updateScoreDisplay(players, current_turn_score);

                            // Roll again
                            if (config->gameMode.value == MODE_FUJINET ) fujinetSendAnimation(ANIMATION_ROLL);
                            farkle_sound_play(SOUND_DICE_ROLL);
                            animateRolling();

                            msx_color(INK_DARK_BLUE,INK_CYAN,INK_CYAN);
                            drawBox(1, 7, 20, 13, true); // clear game board

                            for (i=0; i<6; i++) {
                                if (isDieSaved(current_dice_roll.dice[i]) == false) {
                                    rollDie(current_dice_roll.dice[i]);
                                    placeDieOnBoard(i+1, current_dice_roll.dice[i]);
                                }
                                else {
                                    current_dice_roll.dice[i].locked = true; // Lock the die from future tallys
                                }
                                // is all locked?  unlock and roll fresh
                                if (isAllLocked(current_dice_roll.dice)) {
                                    // status("Good job! Keep rolling?");
                                    _play(3);
                                    //TODO: make this a function, we use it more than once
                                    for (i=0; i<6; i++) {
                                        current_dice_roll.dice[i].locked = false;
                                        current_dice_roll.dice[i].saved = false;
                                        removeDie(current_dice_roll.dice[i]);
                                    }
                                    drawBank(current_dice_roll.dice);                            

                                    for (i=0;i<6;i++) {
                                        rollDie(current_dice_roll.dice[i]);
                                        placeDieOnBoard(i+1, current_dice_roll.dice[i]);
                                    }

                                }
                            }

                            //Send Current dice values
                            if (config->gameMode.value == MODE_FUJINET && current_player == PLAYER_1) fujinetSendDice(current_dice_roll.dice);

                            current_dice_roll.roll_counter++;
                            checkValid(current_dice_roll.dice, false, &valid_turn);
                            if (valid_turn.valid == false) { // Checks for a farkle
                                
                                players[current_player].farkle_count++;

                                // Handle Game Rule 3 farkle Penalty
                                if (config->penalty.isChecked == true && players[current_player].farkle_count >= 3) {
                                    players[current_player].farkle_count = 0;
                                    players[current_player].score = players[current_player].score - 1000;

                                    if (config->gameMode.value == MODE_FUJINET ) fujinetSendAnimation(ANIMATION_THREE_FARKLE);
                                    animateThreeFarkle();

                                }
                                else {

                                    if (config->gameMode.value == MODE_FUJINET ) fujinetSendAnimation(ANIMATION_FARKLE);
                                    animateFarkle();
                                }

                                //TODO: This needs to be a function
                                // Change Player
                                current_dice_roll.roll_counter = 0;
                                current_turn_score = 0;

                                for (i=0; i<6; i++) {
                                    current_dice_roll.dice[i].locked = false;
                                    current_dice_roll.dice[i].saved = false;
                                    removeDie(current_dice_roll.dice[i]);
                                }
                                drawBank(current_dice_roll.dice);
                                turn_mode=TURN_MODE_ROLL;
                                menuTurnOption(turn_mode);

                                if (config->gameMode.value == MODE_FUJINET && current_player == PLAYER_1) fujinetSendScore(players, current_turn_score);

                                current_player = (current_player == PLAYER_1 ? PLAYER_2 : PLAYER_1); // alternate between players

                                drawCurrentPlayer(players, current_player);


                            }
                            else {                                
                                checkToxic(current_dice_roll.dice, &valid_turn);
                                if (valid_turn.valid == false && config->toxic.isChecked == true) {
                                    // Toxic Twos 
                                    if (config->gameMode.value == MODE_FUJINET ) fujinetSendAnimation(ANIMATION_TOXIC);
                                    animateToxicTwos(); // vomit twos!
                                    
                                    //TODO: This needs to be a function!
                                    // Change Player
                                    current_dice_roll.roll_counter = 0;
                                    current_turn_score = 0;

                                    for (i=0; i<6; i++) {
                                        current_dice_roll.dice[i].locked = false;
                                        current_dice_roll.dice[i].saved = false;
                                        removeDie(current_dice_roll.dice[i]);
                                    }
                                    drawBank(current_dice_roll.dice);
                                    turn_mode=TURN_MODE_ROLL;
                                    menuTurnOption(turn_mode);

                                    if (config->gameMode.value == MODE_FUJINET && current_player == PLAYER_1) fujinetSendScore(players, current_turn_score);

                                    current_player = (current_player == PLAYER_1 ? PLAYER_2 : PLAYER_1); // alternate between players
                                    drawCurrentPlayer(players, current_player);

                                }
                                else{
                                    status(" ");
                                }
                                
                            }


                        }
                        else {
                            // Pass
                            int pending_score = current_turn_score + tallyRollScore(current_dice_roll.dice, current_dice_roll.roll_counter, config);
                                             
                            if (pending_score >= 500 || players[current_player].initialFiveHundred == true) {  
                                players[current_player].initialFiveHundred = true;
                                players[current_player].score = players[current_player].score + pending_score;

                                current_dice_roll.roll_counter = 0;
                                current_turn_score = 0;
                                players[current_player].farkle_count = 0;

                                for (i=0; i<6; i++) {
                                    current_dice_roll.dice[i].locked = false;
                                    current_dice_roll.dice[i].saved = false;
                                    removeDie(current_dice_roll.dice[i]);
                                }
                                drawBank(current_dice_roll.dice);
                                turn_mode=TURN_MODE_ROLL;
                                menuTurnOption(turn_mode);

                                current_player = (current_player == PLAYER_1 ? PLAYER_2 : PLAYER_1); // alternate between players

                                if (config->gameMode.value == MODE_FUJINET && current_player == PLAYER_1) fujinetSendScore(players, current_turn_score);
                                drawCurrentPlayer(players, current_player);

                            }
                            else {
                                status ("\n  You must bank at least 500 points to begin.");
                            }
                        }
                    }
                break;
            case 0xF1:  // bypass local processing for online player 2
                break;
            default:
                valid_key = false; 
        
        }
        if (valid_key) {
            drawBank(current_dice_roll.dice);
            if (config->gameMode.value == MODE_FUJINET && current_player == PLAYER_1) fujinetSendScore(players, current_turn_score);
            updateScoreDisplay(players, current_turn_score);


        }
        else {
            smartkeys_sound_play(SOUND_NEGATIVE_BUZZ);
        }
        //TODO: Add config option for winning score?
        if (players[PLAYER_1].score >= 10000 || players[PLAYER_2].score >= 10000) {
            // Winner!!!
            int winner = (players[PLAYER_1].score > players[PLAYER_2].score ? PLAYER_1 : PLAYER_2);
            gameover = true;
            animateWinner(players[winner].name);
            if (config->gameMode.value == MODE_FUJINET ) fujinetSendAnimation(ANIMATION_WINNER);
        }


    }

}