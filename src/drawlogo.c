/**
 * @file drawlogo.c
 * @author John Wohlers (john@wohlershome.net)
 * @brief 
 * @version 0.1
 * @date 2022-02-22
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include "drawlogo.h"
#include "util.h"

void drawLogo(int x, int y, int color) {

    msx_color(color,INK_CYAN,INK_CYAN);
    gotoxy(x, y); 
    cputs("\x88\x89\x8A\x8B\x8C\x8D\x8E\x8F");
    gotoxy(x, y+1); 
    cputs("\x90\x91\x92\x93\x94\x95\x96\x97");
    cputs("FARKLE");

}


void drawBigFarkle(int x, int y, int color) {

    msx_color(color,INK_CYAN,INK_CYAN);
    gotoxy(x, y); 
    cputs("\xB0\xB1\xB2\xB3\xB4\xB5");
    gotoxy(x, y+1); 
    cputs("\xB6\xB7\xB8\xB9\xBA\xBB");

}