/**
 * @file util.c
 * @author John Wohlers (john@wohlershome.net)
 * @brief  Utility functions
 * @version 0.1
 * @date 2022-02-16
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include "util.h"
#include <stdlib.h>
#include <smartkeys.h>
#include <eos.h>


/**
 * @brief Smartkeys Status display. 
 * @author Thom Cherryhomes
 * 
 * @param s 
 */
void status(char *s)
{
  smartkeys_display(NULL,NULL,NULL,NULL,NULL,NULL);
  smartkeys_status(s);
}

/**
 * @brief Temporary debug function to print an integer on screen
 * 
 * @param num 
 */
void debug(int num) {
  gotoxy(0,0);
  char debug[10];
  sprintf(debug, "0x%02X", num);
  cputs(debug);
}

/**
 * @brief Temporary debug function to print an integer on screen
 * 
 * @param num 
 */
void debugDie(Die *dieObj) {
  gotoxy(8,0);
  cprintf("DIE ROLL %d: %d (%d)\n", dieObj->timesRolled, dieObj->value, dieObj->saved);
}


/**
 * @brief returns the last n characters of a string
 * 
 * @param str 
 * @param n 
 * @return char* 
 */
char *lastNchar(const char *str, int n)
{
    int len = strlen(str);

    return (char *)( len < n ? str : str + len - n);
}


int subInt(char *message) {
  char buffer[8];
  memcpy(buffer, &message[2], strlen(message) -2);

  return atoi(buffer);
}