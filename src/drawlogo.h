/**
 * @file drawlogo.h
 * @author John Wohlers (john@wohlershome.net)
 * @brief 
 * @version 0.1
 * @date 2022-02-22
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef DRAWLOGO_H
#define DRAWLOGO_H

#include "util.h"
#include <msx.h>
#include <sys/ioctl.h>
#include <conio.h>
#include <stdlib.h>

/**
 * @brief 
 * 
 * @param x 
 * @param y 
 */
void drawLogo(int x, int y, int color);

/**
 * @brief 
 * 
 * @param x 
 * @param y 
 * @param color 
 */
void drawBigFarkle(int x, int y, int color);

#endif