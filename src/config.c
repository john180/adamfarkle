/**
 * @file config.c
 * @author John Wohlers (john@wohlershome.net)
 * @brief 
 * @version 0.1
 * @date 2022-03-02
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include "config.h"
#include "fujinet.h" // needs to be here instead of in the .h  due to needing the GameConfig struct definition.


void initGameConfig(GameConfig *config) {
    char pip_selected = '\x9C';
    char pip_unselected = '\x9D';

    char mode_labels[3][31] = {{"COMPUTER"}, {"FACE TO FACE"}, {"ONLINE"}};
    int modes;

    // Only display the FujiNet radio option if FujiNet is detected
    if (isFujiNet()) {
        modes = 3;
    }
    else {
        modes = 2;
    }

    initRadioOptionGroup(2,13,INK_WHITE,INK_BLACK,INK_DARK_BLUE, "\0", 0, modes, 1, mode_labels, pip_selected, pip_unselected, config->gameMode);

    char scoring_labels[2][31] = {{"3 OF A KIND MULTIPLIER"}, {"1000/2000/3000 POINTS"}};
    initRadioOptionGroup(2,5,INK_BLACK,INK_WHITE,INK_DARK_BLUE,"SET SCORING", 0, 2, 1, scoring_labels, pip_selected, pip_unselected, config->setScoring);

    char pf_labels[2][31] = {{"1000 POINTS"}, {"300 POINTS"}};
    initRadioOptionGroup(2,9,INK_BLACK,INK_WHITE,INK_DARK_BLUE,"POCKET FARKLE VALUE", 0, 2, 1, pf_labels, pip_selected, pip_unselected, config->pocketFarkle);

    initCheckOption(3,14,INK_BLACK,INK_WHITE,"3 FARKLE PENALTY", true, config->penalty);
    initCheckOption(3,15,INK_BLACK,INK_WHITE,"TOXIC TWOS", false, config->toxic);

}

/**
 * @brief Options screen loop
 * 
 * @param configs 
 */
void optionsLoop(GameConfig *config) {
    char keypress = '\0';
    bool valid_key;


    drawGameOptionsScreen(config);    
    smartkeys_display("    SET\n  \x1F\x1FSCORING", " POCKET\n FARKLE", " FARKLE\n\x1F\x1FPENALTY", " \x1F\x1F\x1FTOXIC\n  TWOS", NULL, "  \x1F\x1FMAIN\n  MENU");

	while (keypress != SMARTKEY_VI)
	{
        keypress = eos_read_keyboard();
        valid_key = true; // true unless we fall out to default on switch, meaning invalid input

        switch(keypress)
        {
            case SMARTKEY_I:
                toggleRadioOption(config->setScoring);
                smartkeys_sound_play(SOUND_KEY_PRESS);
                break;
            case SMARTKEY_II:
                toggleRadioOption(config->pocketFarkle);
                smartkeys_sound_play(SOUND_KEY_PRESS);
                break;
            case SMARTKEY_III:
                toggleCheckOption(config->penalty);
                smartkeys_sound_play(SOUND_KEY_PRESS);
                break;
            case SMARTKEY_IV:
                toggleCheckOption(config->toxic);
                smartkeys_sound_play(SOUND_KEY_PRESS);
                break;
            case SMARTKEY_VI:
                smartkeys_sound_play(SOUND_MODE_CHANGE);
                break;    
            default:
                valid_key = false;
                break;
        }
        if (valid_key == false) {
            smartkeys_sound_play(SOUND_NEGATIVE_BUZZ);
        }
	}

}