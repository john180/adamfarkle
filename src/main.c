/**
 * @file main.c
 * @author John Wohlers (john@wohlershome.net)
 * @brief ADAM Farkle Game Main Logic
 * @version 0.1
 * @date 2022-02-22
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include "sound.h"
#include <msx.h>
#include <sys/ioctl.h>
#include <conio.h>
#include <smartkeys.h>
#include <eos.h>
#include <stdlib.h>
#include "font.h"
#include "drawbox.h"
#include "util.h"
#include "dice.h"
#include "drawlogo.h"
#include "scoreboard.h"
#include "screens.h"
#include "gamelogic.h"
#include "fujinet.h"
#include "player.h"
#include "config.h"
#include "mainmenu.h"
#include <intrinsic.h>
#include <interrupt.h>
#include <arch/z80.h>


// Set an interrupt to count from the moment the program starts to create a psudo random seed based on user input times
unsigned int seed_counter;
static void farkle_seed_counter(void)
{
  M_PRESERVE_ALL;
  if (seed_counter > 65000) seed_counter = 0;
  seed_counter = seed_counter + 1;  
  M_RESTORE_ALL;
}

/**
 * @brief 
 * 
 */
void main(void)
{	

    bool valid_key = true;
    char keypress = '\0';
	char hostname[256];


	Player players[MAX_PLAYERS];
    GameConfig config;

    memset(players,0,sizeof(players));
    memset(config,0,sizeof(config));
    memset(seed_counter,0,sizeof(seed_counter));

    add_raster_int(farkle_seed_counter);

    initPlayers(players);
    initGameConfig(config);


    setupFont();
    smartkeys_set_mode();

    //TODO: Replace with our own sound routine
	//smartkeys_sound_init();	

    farkle_sound_init();

    //farkle_sound_play(SOUND_THEME);

    mainMenuLoop(config, players);

//	if (isFujiNet()) {
//		srand(fujinetRandomNumber());  // Seed with random number from fujinet
//	}
//	else {
        // Set the seed based on the seed counter interrupt
        srand(seed_counter);
//	}


    while (true) {
        players[PLAYER_1].score = 0;
        players[PLAYER_2].score = 0;
            
        if (valid_key) {
            farkle_sound_play(SOUND_SPROING);
            drawGameBoard();
            drawPlayerNames(players);
            updateScoreDisplay(players,0);

            menuTurnOption(TURN_MODE_ROLL);
            
            gameLoop(players, config);

            smartkeys_display("\x1F\x1F\x1F   PLAY\n\x1F\x1F\x1F   AGAIN", "  GAME\n OPTIONS", NULL, NULL, NULL, "\x1F\x1F  EXIT");
        }    
        keypress = eos_read_keyboard();

        switch(keypress)
        {
            case SMARTKEY_I:
                valid_key = true;
                break;

            case SMARTKEY_II:
                valid_key = true;
                mainMenuLoop(config, players);
                break;

            case SMARTKEY_VI:
                valid_key = true;
                eos_exit_to_smartwriter();
                break;

            default:
                valid_key = false;
                smartkeys_sound_play(SOUND_NEGATIVE_BUZZ);
                break;
        }
    }
}