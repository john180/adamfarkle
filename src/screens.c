/**
 * @file screens.c
 * @author John Wohlers (john@wohlershome.net)
 * @brief 
 * @version 0.1
 * @date 2022-02-22
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include "screens.h"
#define INSTRUCTIONS_LENGTH 27

static char instructions_arr[INSTRUCTIONS_LENGTH][29] = { //28 char plus null
       //1234567890123456789012345678
        "HOW TO PLAY:",
        "Turns begin by rolling six",
        "dice. Dice are banked to",
        "build a turn score. You must",
        "set aside at least one die",
        "per roll. Non-banked dice",
        "may be re-rolled to build a",
        "higher score. Bank all six",
        "to continue rolling six more",
        "dice. If no bankable dice ",
        "are rolled, you FARKLE and",
        "your turn ends.",
        "BANKABLE DIE:",
        "1's, 5's, 3/4/5/6 of a kind,",
        "3 pairs in any combination,",        
        "1-2-3-4-5-6 (straight).",
        "NOTE: Scoring combinations",
        "only count when made on a ",
        "single roll. Ex: You cannot",
        "build to a straight.",
        "Pass the dice to the next",
        "player to save your points.",
        "Play to 10,000 points.",
        "CONTROLS:",
        "[TAB] Toggle Roll or Pass",
        "[RETURN] Roll, or Pass",
        "[I]-[VI] Toggle banked die"
};

static unsigned short instructions_color_arr[INSTRUCTIONS_LENGTH] = {
    INK_WHITE, 
    INK_BLACK,
    INK_BLACK,
    INK_BLACK,
    INK_BLACK,
    INK_BLACK,
    INK_BLACK,
    INK_BLACK,
    INK_BLACK,
    INK_BLACK,
    INK_BLACK,
    INK_BLACK,
    INK_WHITE, 
    INK_BLACK,
    INK_BLACK,
    INK_BLACK,
    INK_BLACK,
    INK_BLACK,
    INK_BLACK,
    INK_BLACK,
    INK_BLACK,
    INK_BLACK,
    INK_BLACK, 
    INK_WHITE, 
    INK_BLACK,
    INK_BLACK,
    INK_BLACK, 
};

/**
 * @brief 
 * 
 * @param start_line 
 * @return short   (Returns the recomputed start line in case we are asked to go past the instructions)
 */
short drawInstructions(short start_line) {
    //instructions window 7 x 28    
    short text_line;
    short window_line = 0;
    if (start_line > INSTRUCTIONS_LENGTH - 7) {
        start_line = INSTRUCTIONS_LENGTH - 7;
        gotoxy(29,10);
        msx_color(INK_DARK_BLUE,INK_CYAN,INK_CYAN);
        cputs("\x85"); // remove more indicator
    }
    else {
        gotoxy(29,10);
        msx_color(INK_WHITE,INK_CYAN,INK_CYAN);
        cputs("\x99"); // More indicator
    }

    if (start_line == 0) {
        gotoxy(29,2);
        msx_color(INK_DARK_BLUE,INK_CYAN,INK_CYAN);
        cputs("\x84"); // Remove Less indicator
    }
    else {
        gotoxy(29,2);
        msx_color(INK_WHITE,INK_CYAN,INK_CYAN);
        cputs("\x98"); // Show LESS indicator
    }

    
    for (text_line = start_line; text_line < start_line + 7; text_line++) {
        msx_color(instructions_color_arr[text_line],INK_CYAN,INK_CYAN);
        gotoxy(2,window_line + 3);        
        cprintf("%-28s", instructions_arr[text_line]);
        window_line++;
    }
    return start_line;
}

/**
 * @brief 
 * 
 */
void drawMainMenu(GameConfig *config){

    clrscr();
    drawLogo(2,0, INK_WHITE);
    
    msx_color(INK_DARK_BLUE,INK_CYAN,INK_CYAN);
    drawBox(1,2,30,9,false); // Instructions

    msx_color(INK_WHITE,BOARD_COLOR,BORDER_COLOR);
    gotoxy(2,11);
    cputs("GAME TYPE");
    msx_color(INK_DARK_BLUE,INK_CYAN,INK_CYAN);
    drawBox(1,12,30,5,false); // Game Mode

    msx_color(INK_WHITE,BOARD_COLOR,BORDER_COLOR);
    gotoxy(2,17);
    cputs("YOUR NAME");  //TODO: Make this a function call to display text based on game mode?
    msx_color(INK_DARK_BLUE,INK_CYAN,INK_CYAN);
    drawBox(1,18,30,3,false); // Player Name

    drawRadioOptionGroup(config->gameMode);

}    

void drawGameBoard(void){

    clrscr();
    drawLogo(2,0, INK_WHITE);
    
    msx_color(INK_DARK_BLUE,INK_CYAN,INK_CYAN);
    drawBox(1,2,20,5,false);
    drawDie(2, 3, 1, INK_WHITE);
    drawDie(5, 3, 2, INK_WHITE);
    drawDie(8, 3, 3, INK_WHITE);
    drawDie(11, 3, 4, INK_WHITE);
    drawDie(14, 3, 5, INK_WHITE);
    drawDie(17, 3, 6, INK_WHITE);

    msx_color(INK_WHITE,INK_BLACK,INK_CYAN);
    smartkeys_putc(24,45,4); // I
    smartkeys_putc(48,45,5); // II
    smartkeys_putc(72,45,6); // III
    smartkeys_putc(96,45,7); // IV
    smartkeys_putc(120,45,8); // V
    smartkeys_putc(144,45,9); // VI

    msx_color(INK_DARK_BLUE,INK_CYAN,INK_CYAN);

    drawBox(21,2,10,13,false);
    
    msx_color(INK_BLACK,INK_CYAN,INK_CYAN);
    gotoxy(23,3);
    cputs("SCORES");
    msx_color(INK_DARK_BLUE,INK_CYAN,INK_CYAN);
    gotoxy(22,4);
    cputs("\x84\x84\x84\x84\x84\x84\x84\x84");
    gotoxy(22,7);
    cputs("\x84\x84\x84\x84\x84\x84\x84\x84");
    gotoxy(22,10);
    cputs("\x84\x84\x84\x84\x84\x84\x84\x84");
    msx_color(INK_BLACK,INK_CYAN,INK_CYAN);
    gotoxy(23,11);
    cputs("Current");
    gotoxy(24,12);
    cputs("Turn");

    // playfield
    drawPlayField();

    drawBox(21,15,10,5,false);
    smartkeys_display( NULL, NULL, NULL, NULL, NULL, NULL);
    status("\n  Press [RETURN] to roll.");

}

/**
 * @brief Draws the playfield, also used for clearing the playfield after animations
 * 
 */
void drawPlayField(void) {
    // playfield
    msx_color(INK_DARK_BLUE,INK_CYAN,INK_CYAN);
    drawBox(1,7,20,13,true);
}

/**
 * @brief Clears the screen and draws the game options screen.
 * 
 * @param config 
 */
void drawGameOptionsScreen(GameConfig *config) {

    clrscr();

    drawLogo(2,0, INK_WHITE);
    
    msx_color(INK_DARK_BLUE,INK_CYAN,INK_CYAN);
    drawBox(1,2,30,18,false);

    gotoxy(2,4);
    cputs("\x84\x84\x84\x84\x84\x84\x84\x84\x84\x84\x84\x84");

    msx_color(INK_BLACK,INK_CYAN,INK_CYAN);
    gotoxy(2,3);
    cputs("GAME OPTIONS");

    msx_color(INK_DARK_BLUE,INK_CYAN,INK_CYAN);
    gotoxy(2,13);
    cputs("OPTIONAL RULES");

    //config->setScoring.textColor = INK_DARK_BLUE;
    drawRadioOptionGroup(config->setScoring);
    drawRadioOptionGroup(config->pocketFarkle);
    drawCheckOption(config->penalty);
    drawCheckOption(config->toxic);


}

/**
 * @brief Clears the screen and draws the FujiNet Opponent configuration screen
 * 
 * @param config 
 */
void drawFujiOpponentScreen(GameConfig *config){

	msx_color(INK_DARK_BLUE,BOARD_COLOR,BORDER_COLOR);
	clrscr();
	drawLogo(2,0, INK_WHITE);  //display logo

	msx_color(INK_DARK_BLUE,BOARD_COLOR,BORDER_COLOR);
	drawBox(1,2,30,6,true);
	drawBox(1,10,30,4,false);
	drawBox(1,16,30,3,false);
   	msx_color(INK_WHITE,BOARD_COLOR,BORDER_COLOR);
	gotoxy(2, 3); 
	cputs("Online play requires TCP");
	gotoxy(2, 4); 
	cputs("port 6502 forwarded to the");
	gotoxy(2, 5); 
	cputs("FujiNet device from your");
	gotoxy(2, 6); 
	cputs("firewall / router.");
	gotoxy(2, 9); 
	cputs("CONNECTION MODE");
    gotoxy(2, 15);
	cputs("OPPONENT HOSTNAME / IP");

    drawRadioOptionGroup(config->fujiNetMode);

}

void drawPlayerNames(Player *players) {
    char dispname[9];

    strncpy(dispname, players[PLAYER_1].name, 8);
    dispname[8] = 0;
    msx_color(INK_BLACK,INK_CYAN,INK_CYAN);
    gotoxy(22,5);
    cprintf("%s", dispname);

    strncpy(dispname, players[PLAYER_2].name, 8);
    dispname[8] = 0;

    gotoxy(22,8);
    cprintf("%s", dispname);
}

void drawCurrentPlayer(Player *players, short current_player) {
    msx_color(INK_BLACK,INK_CYAN,INK_CYAN);
    gotoxy(1,20);
    cputs("                        ");
    gotoxy(1,20);
    cprintf("%s's turn", players[current_player].name);
}

void drawMainMenuPlayerName(Player *players, short current_player) {
    msx_color(INK_BLACK,INK_CYAN,INK_CYAN);
    gotoxy(2,19);
    cputs("                        ");
    gotoxy(2,19);
    cprintf("%s ", players[current_player].name);

}

/**
 * @brief TODO: Replace this with the standard radio button code
 * 
 * @param mode 
 */
void menuTurnOption(int mode) {

    msx_color(INK_BLACK,INK_CYAN,INK_CYAN);
    gotoxy(23,16);
    cputs(" Roll");
    gotoxy(23,18);
    cputs(" Pass");

    msx_color(INK_WHITE,INK_CYAN,INK_CYAN);
    if (mode == TURN_MODE_PASS ) {
        gotoxy(22,16);
        cputs(" ");
        gotoxy(22,18);
        cputs("\xAF");

    }
    else if (mode == TURN_MODE_ROLL )
    {
        gotoxy(22,16);
        cputs("\xAF");
        gotoxy(22,18);
        cputs(" ");

    }

}

/**
 * @brief 
 * 
 * @param dice 
 */
void drawBank(Die *dice) {
    int i;
    for (i=0;i<6;i++) {
        if (!dice[i].saved == false) {
            drawDie(dice[i].x, 3, dice[i].value, dice[i].color); // draw the saved dice only
            dice[i].visible = true;
        }
        else {
            drawDie(2 + (i * 3 ), 3, 0, INK_GRAY);
        }

    }

    msx_color(INK_WHITE,INK_BLACK,INK_CYAN);
    smartkeys_putc(24,45,4); // I
    smartkeys_putc(48,45,5); // II
    smartkeys_putc(72,45,6); // III
    smartkeys_putc(96,45,7); // IV
    smartkeys_putc(120,45,8); // V
    smartkeys_putc(144,45,9); // VI


}

/**
 * @brief Animation for Toxic Twos event
 * 
 */
void animateToxicTwos(void) {
    int tt_x;
    int tt_y;    
    int i;
    farkle_sound_play(SOUND_TOXIC_TWOS);

    for (i=0; i<150; i++) {
        tt_x = (rand() % 18) + 2; // Random location for x
        tt_y = (rand() % 11) + 8; // Random location for y

        gotoxy(tt_x,tt_y);
        msx_color(INK_LIGHT_GREEN,INK_CYAN,INK_CYAN);
        cputs("\xAF");

        gotoxy(6,13);
        msx_color(INK_BLACK,INK_LIGHT_GREEN,INK_CYAN);
        cputs("ToXiC TwOs");

    }

    sleep(3); // give the sound a moment to play 
    drawPlayField(); // clear the field
}

void animateThreeFarkle(void) {

    drawPlayField();  // Clear the play field

    for (int f=8;f<=17;f++) {
        if (f > 8) {
        drawBigFarkle(9, f-1, INK_CYAN);
        }
        drawBigFarkle(9,f, INK_DARK_RED);
        csleep(5);
    }
    smartkeys_sound_play(SOUND_OOPS);
    sleep(1);
    for (int f=8;f<=14;f++) {
        if (f > 8) {
        drawBigFarkle(9,f-1, INK_CYAN);    
        }
        drawBigFarkle(9,f, INK_DARK_RED);                    
        csleep(5);
    }
    smartkeys_sound_play(SOUND_OOPS);
    sleep(1);
    for (int f=8;f<=11;f++) {
        if (f > 8) {
        drawBigFarkle(9,f-1, INK_CYAN);    
        }
        drawBigFarkle(9,f, INK_DARK_RED);                    
        csleep(5);
    }
    smartkeys_sound_play(SOUND_OOPS);
    gotoxy(3,8);
    msx_color(INK_DARK_RED,INK_CYAN,INK_CYAN);                
    cputs("3 Farkle Penalty");
    gotoxy(5,9);
    cputs("-1000 points");

    sleep(5); // leave the message up for a bit
    drawPlayField(); // Clear the play field

}

/**
 * @brief Draw Rolling animation
 * 
 */
void animateRolling() {

    msx_color(INK_DARK_BLUE,INK_CYAN,INK_CYAN);                            
    drawBox(1, 7, 20, 13, true); // clear game board

    gotoxy(8,13);
    msx_color(INK_BLACK,INK_CYAN,INK_CYAN);
    cputs("ROLLING");
    for (int i = 0; i < 100; i++) {
        drawDie(5, 12, (rand() % 6) + 1, INK_WHITE);
        drawDie(15, 12, (rand() % 6) + 1, INK_WHITE);
    }
    msx_color(INK_DARK_BLUE,INK_CYAN,INK_CYAN);                            
    drawPlayField(); // Clear the play field
}

/**
 * @brief Draw Winner screen
 * 
 * @param name 
 */
void animateWinner(char name[15]) {
    int x;

    //TODO:  Jazz this up a bit, they won make a bigger deal out of it.
    gotoxy(1,20);
    cprintf("%-30s", " ");

    for (int y = 16; y >= 10; y--) {
        if (y < 16) {
            drawDie(2, y , NULL, INK_CYAN);
            drawDie(2, y -1, NULL, INK_WHITE);
            drawDie(5, y +1, NULL, INK_CYAN);
            drawDie(5, y, NULL, INK_WHITE);
            drawDie(8, y , NULL, INK_CYAN);
            drawDie(8, y -1, NULL, INK_WHITE);
            drawDie(11, y +1, NULL, INK_CYAN);
            drawDie(11, y, NULL, INK_WHITE);
            drawDie(14, y , NULL, INK_CYAN);
            drawDie(14, y -1, NULL, INK_WHITE);
            drawDie(17, y +1, NULL, INK_CYAN);
            drawDie(17, y, NULL, INK_WHITE);                
        }
        else {
            drawDie(2, y -1, NULL, INK_WHITE);
            drawDie(5, y, NULL, INK_WHITE);
            drawDie(8, y -1, NULL, INK_WHITE);
            drawDie(11, y, NULL, INK_WHITE);
            drawDie(14, y -1, NULL, INK_WHITE);
            drawDie(17, y, NULL, INK_WHITE);                
        }
        //csleep(5);
    }

    msx_color(INK_DARK_RED,INK_WHITE,INK_CYAN);                
    gotoxy(3,10);
    cputs("W");
    gotoxy(6,11);
    cputs("I");
    gotoxy(9,10);
    cputs("N");
    gotoxy(12,11);
    cputs("N");
    gotoxy(15,10);
    cputs("E");
    gotoxy(18,11);
    cputs("R");

    size_t len = strlen(name);
    x = 9 - ((len) / 2) + 2;

    gotoxy(x,14);
    msx_color(INK_DARK_YELLOW,INK_BLACK,INK_BLACK);

    cprintf("%s", name);
}

/**
 * @brief 
 * 
 */
void animateFarkle() {
    
    farkle_sound_play(SOUND_FARKLED);

    //TODO: Do this as a sprite? would eliminate character replacement over dice corners
    drawBigFarkle(9,12, INK_DARK_RED); 

    sleep(5); // leave the message up for a bit while sound plays
    drawPlayField(); // Clear the play field

}

/**
 * @brief 
 * 
 * @param animation 
 */
void doAnimation(int animation) {
    switch (animation) {
        case ANIMATION_ROLL:
            animateRolling();
            break;
        case ANIMATION_FARKLE:
            animateFarkle();
            break;
        case ANIMATION_TOXIC:
            animateToxicTwos();
            break;
        case ANIMATION_THREE_FARKLE:
            animateThreeFarkle();
            break;             
    }
}
