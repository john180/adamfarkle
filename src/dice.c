/**
 * @file dice.c
 * @author John Wohlers (john@wohlershome.net)
 * @brief 
 * @version 0.1
 * @date 2022-02-22
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include "dice.h"

/**
 * @brief Draw a die in a specific position, with a value
 * 
 * @param x 
 * @param y 
 * @param val 
 * @param color
 */
void drawDie(int x, int y, int val, int color) {
  msx_color(color,INK_CYAN,INK_CYAN);

  gotoxy(x, y); 
  cputs("\xA0\xA4\xA1"); // top row
  gotoxy(x, y + 1); 
  cputs("\xA6\x20\xA7"); // middle row
  gotoxy(x, y + 2); 
  cputs("\xA2\xA5\xA3"); // bottom row

  msx_color(INK_BLACK,color,INK_CYAN);
  gotoxy(x + 1, y + 1); 
  //Used Switch to allow for future possible dice faces...
  switch (val)
  {
  case 1/* constant-expression */:
      /* code */
      cputs("\xA8"); // Pip 1
      break;
  case 2/* constant-expression */:
      /* code */
      cputs("\xA9"); // Pip 2
      break;
  case 3/* constant-expression */:
      /* code */
      cputs("\xAA"); // Pip 3
      break;
  case 4/* constant-expression */:
      /* code */
      cputs("\xAB"); // Pip 4
      break;
  case 5/* constant-expression */:
      /* code */
      cputs("\xAC"); // Pip 5
      break;
  case 6/* constant-expression */:
      /* code */
      cputs("\xAD"); // Pip 6
      break;
  
  default:
      cputs("\x20"); // blank
      break;
  }

}

/**
 * @brief 
 * 
 * @param col 
 * @param dieObj 
 * @return int 
 */
int placeDieOnBoard(int col, Die *dieObj) {

    // x = left most position + column - 1 * dice width
    dieObj->x = 2 + ((col - 1) * 3 ); // Set dieObj->x to position calculated for passed column
    dieObj->saved = false;
    
    drawDie(dieObj->x , dieObj->y, dieObj->value, dieObj->color);
    dieObj->visible = true;
}

/**
 * @brief Set the saved flag, and remove the die from the playing field
 * 
 * @param dieObj 
 */
void saveDie(Die *dieObj) {

    dieObj->saved = true;
    removeDie(dieObj);

}

/**
 * @brief Unset the saved flag, and dar the die back on the playing field
 * 
 * @param dieObj 
 */
void releaseDie(Die *dieObj) {

    dieObj->saved = false;
    drawDie(dieObj->x , dieObj->y, dieObj->value, dieObj->color);

}



/**
 * @brief // Removes the die graphic from the play field
 * 
 * @param dice 
 */
void removeDie(Die *dieObj) {
  msx_color(INK_CYAN,INK_CYAN,INK_CYAN);
  
  gotoxy(dieObj->x, dieObj->y); 
  cputs("   "); // top row
  gotoxy(dieObj->x, dieObj->y + 1); 
  cputs("   "); // middle row
  gotoxy(dieObj->x, dieObj->y + 2); 
  cputs("   "); // bottom row

  dieObj->visible = false;
}


/**
 * @brief  Rolls the die:  Changes value to random and sets its y location to a random height
 * 
 * @param dieObj
 * @return struct die
 */
void rollDie(Die *dieObj) {
    
    dieObj->value = (rand() % 6) + 1; // random value of the die
    dieObj->y = (rand() % 9) + 8; // Random height location on the board
    dieObj->timesRolled++;
}


void resetDie(Die *dieObj) {
    dieObj->color = INK_WHITE;
    dieObj->timesRolled = 0;
    dieObj->value = 0;
    dieObj->saved = false;
    dieObj->locked = false;
    dieObj->visible = false;
    dieObj->x = 0;
    dieObj->y = 0;
}

bool isDieSaved(Die *dieObj) {
    return dieObj->saved;
}

bool isDieLocked(Die *dieObj) {
    return dieObj->locked;
}

bool isAllLocked(Die *dice_group) {
    bool allLocked = true;
    int i;

    for (i=0; i < 6; i++) {
        
        if (dice_group[i].locked == false) {
            allLocked = false;
        }    
    }
    return allLocked;
}



void toggleDieSaveState(Die *dieObj) {
    if (dieObj->visible) {
        if (isDieSaved(dieObj) == false || isDieLocked(dieObj)) {
            saveDie(dieObj);
        }
        else {
            releaseDie(dieObj);
        }
    }

}


void initRoll(Roll *myroll) {
    short i;
    for (i=0;i<6;i++) {
        myroll->counts[i] = 0;
    }
    myroll->roll_counter = 0;
}

/**
 * @brief 
 * 
 * @param dice_group 
 * @return short 
 */
short countBankedDice(Die *dice_group) {
    short bank_count = 0;

    for (short i=0; i < 6; i++) {
        
        if (isDieSaved(dice_group[i])) {
            bank_count++;
        }    
    }

    return (bank_count);

}
